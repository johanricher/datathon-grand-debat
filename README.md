# Utilitaires du Datathon du 23 mars 2019

**Résumé**

Ce dépôt propose un ensemble de scripts Python et R pour faciliter le téléchargement, l'exploration et l'analyse de données relatives au Grand Débat, en vue d'accélérer le travail des participant(e)s de l'évènement.

Étapes :
1. Accédez au dossier avec les données compressées ici : [https://drive.google.com/open?id=1f49KQl8p9zL4FUMn2q2__tBL8oWyCkhI](https://drive.google.com/open?id=1f49KQl8p9zL4FUMn2q2__tBL8oWyCkhI)
2. Téléchargez le fichier, décompressez le, et placez-le à la racine du dossier
2. Explorer ces données avec le notebook Python `./exploration.ipynb`

## Étape 1 - Téléchargement

L'archive est composée de données extraites des sources suivantes :

* Le [Grand Débat National](https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-grand-debat-national/) : plateforme officielle du Grand Débat
* La Grande Annotation : plateforme libre permettant de labelliser les participations individuelle du Grand Débat en vue de les analyser
* Entendre la France : chatbot messenger diffusant les questions du Grand Débat
* Annotation d'Entendre la France : plateforme libre analogue à la Grande Annotation

## Étape 2 - Exploration

Les objectifs des outils proposés sont de :
1. permettre l'exploration conjointe de jeux de données (par exemple Grand Débat et Grande Annotation) ; c'est ce que permet le notebook `./exploration.ipynb`
2. fournir un preprocessing et une première analyse des données ; les notebooks `./notebooks_entendre_la_france`, qui contient notamment un fichier `requirements.txt` pour installer les packages python nécessaires
