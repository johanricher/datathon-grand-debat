# Partage de ressources des groupes 

## Nom des groupes 

- 1/ Grande Annotation (repo lien à créer)
- 2/ Analyse comparative des contributions citoyens : 
- 3/ Outils pour les parlementaires
- 4/ Visualisation de réponses aux questions ouvertes
- 5/ Démocratie.app 
- 6/ OpenDebat 
- 7/ Données vrai débat : https://github.com/c24b/vrai-debat
- 8/ Que pensent mes voisins ?
- 9/ Analyse des contributions Lille https://github.com/OpenSourcePolitics/datathon-grand-debat
- 10/ Navigateur GDN : https://github.com/ValentinRicher/nav-grand-debat
- 11/ Circonscription 360
- 12/ Entendre la France : 
- 13/ Anatomie du Grand Débat 
- 



### Organisation intergroupes 

- Discussion intergroupes : https://chat.codefor.fr/channel/opendebat
- Mise en ligne des slides fin de journée 

### Dataset disponibles

- Grand Débat : [dossier](https://drive.google.com/drive/folders/1f49KQl8p9zL4FUMn2q2__tBL8oWyCkhI)
- Vrai debat : plateforme alternative au Grand Débat National données des propositions par theme disponible dans ce  [dossier](https://cloud.driss.org/index.php/s/muN3N9JzcSRWKpp) Mot de passe: `Parlement-Ouvert` 
- Dataset GDN Lille : https://github.com/OpenSourcePolitics/datathon-grand-debat/tree/master/data  

### Scripts 











